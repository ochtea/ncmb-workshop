package com.ncmb.android.sample.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.ncmb.android.sample.MainActivity.Category;
import com.ncmb.android.sample.R;

public class EditDialogFragment extends DialogFragment {

    public interface EditDialogFragmentCallback {
        public void onEditDone(String text, Category category, int requestId);
    }

    public static final String TAG = "EditDialogFragment";
    private EditText mEditText;

    public static EditDialogFragment newInstance(String message,
            Category category, int requestId) {
        EditDialogFragment frag = new EditDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putInt("category", category.getValue());
        args.putInt("requestId", requestId);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString("message");
        final Category category = Category.getEnum(getArguments().getInt(
                "category"));
        final int requestId = getArguments().getInt("requestId");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        mEditText = (EditText) inflater.inflate(R.layout.dialog_edit, null);

        return new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_menu_edit)
                .setTitle(R.string.user_registration)
                .setMessage(message)
                .setView(mEditText)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                                String name = mEditText.getText().toString();
                                Activity ac = getActivity();
                                if (ac != null) {
                                    ((EditDialogFragmentCallback) ac)
                                            .onEditDone(name, category,
                                                    requestId);
                                }
                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                                EditDialogFragment.this.dismiss();
                            }
                        }).create();
    }

}
