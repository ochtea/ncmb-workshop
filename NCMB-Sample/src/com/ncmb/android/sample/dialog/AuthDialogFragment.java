package com.ncmb.android.sample.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.ncmb.android.sample.MainActivity.Category;
import com.ncmb.android.sample.R;

public class AuthDialogFragment extends DialogFragment {

    public interface UserDialogFragmentCallback {
        public void onInputDone(String username, String password,
                Category category, int requestId);
    }

    public static final String TAG = "UserDialogFragment";
    private EditText mEditTextId;
    private EditText mEditTextPass;

    public static AuthDialogFragment newInstance(String message,
            Category category, int requestId) {
        AuthDialogFragment frag = new AuthDialogFragment();
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putInt("category", category.getValue());
        args.putInt("requestId", requestId);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString("message");
        final Category category = Category.getEnum(getArguments().getInt(
                "category"));
        final int requestId = getArguments().getInt("requestId");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        RelativeLayout layout = (RelativeLayout) inflater.inflate(
                R.layout.dialog_auth, null);
        mEditTextId = (EditText) layout.findViewById(R.id.edit_username);
        mEditTextPass = (EditText) layout.findViewById(R.id.edit_password);

        return new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_menu_edit)
                .setTitle(R.string.user_registration)
                .setMessage(message)
                .setView(layout)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                                String name = mEditTextId.getText().toString();
                                String pass = mEditTextPass.getText()
                                        .toString();
                                Activity ac = getActivity();
                                if (ac != null) {
                                    ((UserDialogFragmentCallback) ac)
                                            .onInputDone(name, pass, category,
                                                    requestId);
                                }
                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                                AuthDialogFragment.this.dismiss();
                            }
                        }).create();
    }

}
