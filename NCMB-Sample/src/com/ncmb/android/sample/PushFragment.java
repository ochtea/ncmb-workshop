package com.ncmb.android.sample;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.ncmb.android.sample.MainActivity.Category;
import com.ncmb.android.sample.dialog.EditDialogFragment;
import com.ncmb.android.sample.util.ToastUtil;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBInstallation;
import com.nifty.cloud.mb.NCMBPush;
import com.nifty.cloud.mb.RegistrationCallback;
import com.nifty.cloud.mb.SendCallback;

public class PushFragment extends Fragment implements OnItemClickListener {

    private static final String TAG = "PushFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.push_menu_list, container, false);
        ListView listView = (ListView) v.findViewById(R.id.pushListView);
        listView.setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
        if (pos == 0) {
            doPushInstallation(this.getActivity().getApplicationContext());
        } else if (pos == 1) {
            NCMBPush.setDefaultPushCallback(getActivity()
                    .getApplicationContext(), MainActivity.class);
        } else if (pos == 2) {
            EditDialogFragment fragment = EditDialogFragment.newInstance(
                    "Input push message", Category.PUSH, pos);
            fragment.show(getFragmentManager(), EditDialogFragment.TAG);
        }
    }

    public static void doPushInstallation(final Context ctx) {
        final NCMBInstallation installation = NCMBInstallation
                .getCurrentInstallation();
        installation.getRegistrationIdInBackground(AppConstants.GCM_SENDER_ID,
                new RegistrationCallback() {
                    @Override
                    public void done(NCMBException registException) {
                        if (registException != null) {
                            registException.printStackTrace();
                            Toast.makeText(
                                    ctx,
                                    "Failed push registration "
                                            + registException.getMessage(),
                                    Toast.LENGTH_LONG).show();
                            return;
                        }

                        Toast.makeText(
                                ctx,
                                "Success push registration, continue to push installation ",
                                Toast.LENGTH_LONG).show();

                        String saveMessage = null;
                        try {
                            installation.save();
                            saveMessage = "Success : push installation";
                        } catch (NCMBException saveException) {
                            saveException.printStackTrace();
                            saveMessage = "Failed : push installation";
                        }
                        Toast.makeText(ctx, saveMessage, Toast.LENGTH_LONG)
                                .show();
                    }
                });
    }

    static void sendPushMessage(final Context ctx, String text) {
        NCMBPush push = new NCMBPush();
        push.setMessage(text);
        push.sendInBackground(new SendCallback() {
            @Override
            public void done(NCMBException sendExp) {
                if (sendExp != null) {
                    String message = "Failed : message sent / "
                            + sendExp.getMessage();
                    Log.v(TAG, message);
                    ToastUtil.showLong(ctx, message);
                    sendExp.printStackTrace();
                    return;
                }
                String message = "Success : message sent";
                Log.v(TAG, message);
                ToastUtil.showShort(ctx, message);
            }
        });
    }
}
