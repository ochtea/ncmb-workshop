package com.ncmb.android.sample;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.ncmb.android.sample.MainActivity.Category;
import com.ncmb.android.sample.dialog.AlertDialogFragment;
import com.ncmb.android.sample.dialog.AuthDialogFragment;
import com.ncmb.android.sample.util.ToastUtil;
import com.nifty.cloud.mb.LogInCallback;
import com.nifty.cloud.mb.NCMBAnonymousUtils;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBUser;
import com.nifty.cloud.mb.SignUpCallback;

public class UserFragment extends Fragment implements OnItemClickListener {

    public static final String TAG = "UserMenuFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.user_menu_list, container, false);
        ListView listView = (ListView) v.findViewById(R.id.userListView);
        listView.setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
        if (pos == 0) {
            AuthDialogFragment fragment = AuthDialogFragment.newInstance(
                    "User register", Category.USER, pos);
            fragment.show(getFragmentManager(), AuthDialogFragment.TAG);
        } else if (pos == 1) {
            AuthDialogFragment fragment = AuthDialogFragment.newInstance(
                    "User login", Category.USER, pos);
            fragment.show(getFragmentManager(), AuthDialogFragment.TAG);
        } else if (pos == 2) {
            loginAnonymousUser(getActivity().getApplicationContext());
        } else if (pos == 3) {
            NCMBUser user = NCMBUser.getCurrentUser();
            String userInfo = null;
            if (user == null) {
                userInfo = "There is no user";
            } else {
                userInfo = "Username : " + user.getUsername() + "\nEmail : "
                        + user.getEmail() + "\nObjectID : "
                        + user.getObjectId();
            }
            AlertDialogFragment fragment = AlertDialogFragment.newInstance(
                    "UserInfo", userInfo);
            fragment.show(getFragmentManager(), AlertDialogFragment.TAG);

        } else if (pos == 4) {
            try {
                NCMBUser.logOut();
                String message = "Success : Logout";
                Log.v(TAG, message);
                ToastUtil.showShort(getActivity().getApplicationContext(),
                        message);
            } catch (NCMBException e) {
                String message = "Failed : Logout";
                Log.v(TAG, message);
                ToastUtil.showLong(getActivity().getApplicationContext(),
                        message);
                e.printStackTrace();
            }
        }
    }

    static void registerUser(final Context ctx, String username, String password) {
        NCMBUser user = new NCMBUser();
        user.setUsername(username);
        user.setPassword(password);

        user.signUpInBackground(new SignUpCallback() {
            public void done(NCMBException signupExp) {
                if (signupExp != null) {
                    String message = "Failed : SignUp / "
                            + signupExp.getMessage();
                    Log.v(TAG, message);
                    ToastUtil.showLong(ctx, message);
                    signupExp.printStackTrace();
                    return;
                }
                String message = "Success : SignUp";
                Log.v(TAG, message);
                ToastUtil.showShort(ctx, message);
            }
        });
    }

    static void loginRegisterdUser(final Context ctx, String username,
            String password) {
        NCMBUser.logInInBackground(username, password, new LogInCallback() {
            public void done(NCMBUser user, NCMBException loginExp) {
                if (loginExp != null || user == null) {
                    String message = "Failed : Login";
                    if (loginExp != null) {
                        message += loginExp.getMessage();
                    }
                    Log.v(TAG, message);
                    ToastUtil.showLong(ctx, message);
                    loginExp.printStackTrace();
                    return;
                }
                String message = "Success : Login";
                Log.v(TAG, message);
                ToastUtil.showShort(ctx, message);
            }
        });
    }

    static void loginAnonymousUser(final Context ctx) {
        NCMBAnonymousUtils.logIn(new LogInCallback() {
            @Override
            public void done(NCMBUser user, NCMBException loginExp) {
                if (loginExp != null || user == null) {
                    String message = "Failed : Anonymous login";
                    if (loginExp != null) {
                        message += loginExp.getMessage();
                    }
                    Log.v(TAG, message);
                    ToastUtil.showLong(ctx, message);
                    loginExp.printStackTrace();
                    return;
                }
                String message = "Success : Anonymous login";
                Log.v(TAG, message);
                ToastUtil.showShort(ctx, message);
            }
        });
    }
}
