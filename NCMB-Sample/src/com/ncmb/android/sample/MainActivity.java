package com.ncmb.android.sample;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.Menu;

import com.ncmb.android.sample.dialog.AuthDialogFragment.UserDialogFragmentCallback;
import com.ncmb.android.sample.dialog.EditDialogFragment.EditDialogFragmentCallback;
import com.nifty.cloud.mb.NCMB;
import com.nifty.cloud.mb.NCMBAnalytics;

public class MainActivity extends FragmentActivity implements
        UserDialogFragmentCallback, EditDialogFragmentCallback {

    private static final String TAG = "MainActivity";

    public enum Category {
        USER(1), PUSH(2);

        private int categoryNum;

        private Category(int num) {
            this.categoryNum = num;
        }

        public int getValue() {
            return categoryNum;
        }

        public static Category getEnum(int num) {
            Category[] enumArray = Category.values();
            for (Category enumInt : enumArray) {
                if (num == enumInt.getValue()) {
                    return enumInt;
                }
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maintab_fragment);

        // Initialize nifty mobile backend
        NCMB.initialize(this, AppConstants.APP_ID, AppConstants.APP_KEY);

        // For push notification tracking
        NCMBAnalytics.trackAppOpened(getIntent());

        // Initialize tabs
        initTabs();
    }

    private void initTabs() {
        FragmentTabHost host = (FragmentTabHost) findViewById(android.R.id.tabhost);
        host.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
        host.addTab(host.newTabSpec("User").setIndicator("User"),
                UserFragment.class, null);
        host.addTab(host.newTabSpec("Push").setIndicator("Push"),
                PushFragment.class, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onInputDone(String username, String password,
            Category category, int requestId) {
        if (category == Category.USER) {
            Log.v(TAG, "Input : " + username + " / " + password);
            if (requestId == 0) {
                UserFragment.registerUser(getApplicationContext(), username,
                        password);
            } else if (requestId == 1) {
                UserFragment.loginRegisterdUser(getApplicationContext(),
                        username, password);
            }
        }
    }

    @Override
    public void onEditDone(String text, Category category, int requestId) {
        if (category == Category.PUSH) {
            Log.v(TAG, "Input : " + text);
            if (requestId == 2) {
                PushFragment.sendPushMessage(getApplicationContext(), text);
            }
        }
    }

}
