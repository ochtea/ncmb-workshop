package com.ncmb.android.sample.util;

import android.content.Context;
import android.widget.Toast;

public class ToastUtil {

    public static final void showShort(Context context, String msg) {
        show(context, msg, Toast.LENGTH_SHORT);
    }

    public static final void showLong(Context context, String msg) {
        show(context, msg, Toast.LENGTH_LONG);
    }

    private static final void show(Context context, String msg, int duration) {
        Toast.makeText(context, msg, duration).show();
    }

}
